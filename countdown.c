///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// Nathaniel Murray <murrayn@hawaii.edu>
// @date   3/18/2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
int main () {

   struct tm *hard_coded_time_ptr, hard_coded_time;
   time_t mktime(struct tm *hard_coded_time_ptr);
   hard_coded_time_ptr = &hard_coded_time;

   hard_coded_time_ptr->tm_sec=7;
   hard_coded_time_ptr->tm_min=26;
   hard_coded_time_ptr->tm_hour=4;
   hard_coded_time_ptr->tm_mday=21;
   hard_coded_time_ptr->tm_mon=0;
   hard_coded_time_ptr->tm_year=2014-1900;
   hard_coded_time_ptr->tm_wday=2;
   hard_coded_time_ptr->tm_yday=365;
   hard_coded_time_ptr->tm_isdst=0;

   printf("Reference Time: %s", asctime(hard_coded_time_ptr));
   printf("Time Zone: HST\n");
  // printf("Seconds since epoch reference: %ld\n", mktime(&hard_coded_time));

   do{
      // printf("Seconds since epoch local: %ld\n", time(NULL));

      long int time_difference_ld = time(NULL) - mktime(&hard_coded_time);

      //printf("Time difference: %ld\n", time_difference_ld);

      long int time_year = time_difference_ld/31536000;
      long int time_year_remain = time_difference_ld % 31536000;

      //printf("time year remain:%ld\n", time_year_remain);

      long int time_day = time_difference_ld/86400;
      long int time_day_remain = time_difference_ld % 86400;

      //printf("time day remain:%ld\n", time_day_remain);

      long int time_hour = time_difference_ld/3600;
      long int time_hour_remain = time_difference_ld % 3600;

      //printf("time hour remain:%ld\n", time_hour_remain);

      long int time_min = time_difference_ld/60;
      long int time_min_remain = time_difference_ld % 60;

      //printf("time min remain:%ld\n", time_min_remain);

      long int time_sec = time_difference_ld;

      if(time_year_remain>0){
         time_day = (time_difference_ld - (time_year*31536000)) / 86400;
      }

      if(time_day_remain>0){
         time_hour = (time_difference_ld - (time_year*31536000) - (time_day*86400)) / 3600;
      }

      if(time_hour_remain>0){
         time_min = (time_difference_ld - (time_year*31536000) - (time_day*86400) - (time_hour*3600)) / 60;
      }

      if(time_min_remain>0){
         time_sec = time_difference_ld - (time_year*31536000) - (time_day*86400) - (time_hour*3600) - (time_min*60);
      }

         sleep(1);           
           printf("Years:%ld Days:%ld Hours:%ld Minutes:%ld Seconds:%ld\n", time_year, time_day, time_hour, time_min, time_sec);
   } while(true);

return 0;
}
